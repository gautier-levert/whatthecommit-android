package org.mybop.whatthecommit

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class)
class DownloadFragmentTest {

    private var downloadFragment: DownloadFragment? = null

    @Before
    fun setUp() {
        downloadFragment = DownloadFragment.newInstance()
    }

    @After
    fun tearDown() {

    }

    @Test
    fun mockTest() {

    }

}
