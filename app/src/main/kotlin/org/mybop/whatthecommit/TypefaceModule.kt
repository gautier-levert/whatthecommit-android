package org.mybop.whatthecommit

import android.content.Context
import android.graphics.Typeface
import toothpick.config.Module

class TypefaceModule(context: Context) : Module() {
    init {
        bind(Typeface::class.java)
                .withName("lucida-console")
                .toInstance(
                        Typeface.createFromAsset(context.assets, "fonts/lucida-console.ttf")
                )
    }
}
