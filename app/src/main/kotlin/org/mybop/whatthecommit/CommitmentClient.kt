package org.mybop.whatthecommit

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Retrofit REST client for <a href="https://whatthecommit.com">whatthecommit.com</a>
 *
 * @author Gautier Levert
 */
interface CommitmentClient {

    /**
     * Get random commit message (like home page)
     * @return http call to get message as String
     */
    @GET("index.txt")
    fun randomMessage(): Single<String>

    /**
     *  Get a specific message by hash (permalink)
     *  @return http call to get message as String
     */
    @GET("{hash}/index.txt")
    fun findMessage(@Path("hash") hash: String): Single<String>
}
