package org.mybop.whatthecommit

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import toothpick.config.Module

/**
 * Toothpick modul for retrofit client injection
 */
object RetrofitModule : Module() {

    init {
        bind(CommitmentClient::class.java)
                .toProviderInstance {
                    Retrofit.Builder()
                            .baseUrl("https://whatthecommit.com/")
                            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                            .addConverterFactory(ScalarsConverterFactory.create())
                            .build()
                            .create(CommitmentClient::class.java)
                }
                .providesSingletonInScope()
    }
}
