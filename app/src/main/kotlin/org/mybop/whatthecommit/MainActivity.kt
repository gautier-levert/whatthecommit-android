package org.mybop.whatthecommit

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.ShareActionProvider
import androidx.core.view.MenuItemCompat
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.get
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import io.reactivex.disposables.CompositeDisposable
import toothpick.Toothpick
import toothpick.smoothie.module.SmoothieActivityModule


class MainActivity : AppCompatActivity() {

    companion object {
        val LOG_TAG: String = MainActivity::class.java.simpleName
    }

    private var shareActionProvider: ShareActionProvider? = null

    @BindView(R.id.main_commit)
    internal lateinit var textView: TextView

    private lateinit var commitViewModel: CommitViewModel

    private val subscription = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Toothpick injection
        val scope = Toothpick.openScopes(application, this)

        scope.installModules(SmoothieActivityModule(this))

        Toothpick.inject(this, scope)

        // Butter Knife injection
        ButterKnife.bind(this)

        // update font of textview
        textView.typeface = scope.getInstance(Typeface::class.java, "lucida-console")

        commitViewModel = ViewModelProviders.of(this).get()

        subscription.add(
                commitViewModel.commit
                        .subscribe({
                            textView.text = it

                            val sendIntent = Intent()
                            sendIntent.action = Intent.ACTION_SEND
                            sendIntent.putExtra(Intent.EXTRA_TEXT, it)
                            sendIntent.type = "text/plain"

                            shareActionProvider?.setShareIntent(sendIntent)
                        }, {
                            Log.e(LOG_TAG, "error", it)
                        })
        )

        commitViewModel.random()
    }

    @OnClick(R.id.floatingActionButton)
    internal fun refresh() {
        Log.d(MainActivity::class.java.simpleName, "click on refresh")
        commitViewModel.random()
    }

    override fun onDestroy() {
        super.onDestroy()
        subscription.dispose()
        Toothpick.closeScope(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.main, menu)

        val menuItemShare = menu.findItem(R.id.menu_item_share)
        shareActionProvider = MenuItemCompat.getActionProvider(menuItemShare) as ShareActionProvider

        return true
    }
}
