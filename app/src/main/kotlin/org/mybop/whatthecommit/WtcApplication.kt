package org.mybop.whatthecommit

import android.app.Application
import toothpick.Toothpick
import toothpick.smoothie.module.SmoothieApplicationModule

/**
 * Application class
 */
class WtcApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        val scope = Toothpick.openScope(this)

        scope.installModules(
                SmoothieApplicationModule(this),
                RetrofitModule,
                TypefaceModule(this)
        )

        Toothpick.inject(this, scope)
    }
}
